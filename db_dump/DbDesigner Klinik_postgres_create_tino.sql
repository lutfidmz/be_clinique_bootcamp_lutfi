CREATE TABLE "employess" (
	"id" varchar(36) NOT NULL,
	"name" varchar(255) NOT NULL,
	"email" varchar(255) NOT NULL UNIQUE,
	"phone" varchar(16) NOT NULL UNIQUE,
	"username" varchar(255) NOT NULL UNIQUE,
	"password" varchar(255) NOT NULL UNIQUE,
	"nik" varchar(16) NOT NULL UNIQUE,
	"address" TEXT NOT NULL,
	"specialization" varchar(255) NOT NULL,
	"role" varchar(255) NOT NULL,
	"join_date" DATE NOT NULL,
	"gender" varchar(1) NOT NULL,
	CONSTRAINT "employess_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "doctors" (
	"id" varchar(36) NOT NULL,
	"employee_id" varchar(36) NOT NULL,
	"specialization" varchar(255) NOT NULL,
	"license_number" varchar(255) NOT NULL,
	"fees" FLOAT NOT NULL,
	CONSTRAINT "doctors_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "pharmacists" (
	"id" varchar(36) NOT NULL,
	"employee_id" varchar(36) NOT NULL,
	"license_number" varchar(255) NOT NULL,
	CONSTRAINT "pharmacists_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "patients" (
	"id" varchar(36) NOT NULL,
	"regist_by" varchar(36) NOT NULL,
	"name" varchar(255) NOT NULL,
	"phone" varchar(16) NOT NULL UNIQUE,
	"email" varchar(255) NOT NULL UNIQUE,
	"address" TEXT NOT NULL,
	"gender" varchar(1) NOT NULL,
	"status" varchar(255) NOT NULL,
	"birthday" DATE NOT NULL,
	"nik" varchar(16) NOT NULL UNIQUE,
	"username" varchar(255) NOT NULL UNIQUE,
	"password" varchar(255) NOT NULL UNIQUE,
	"register_date" TIMESTAMP NOT NULL,
	"is_verified" BOOLEAN NOT NULL,
	CONSTRAINT "patients_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "medical_records" (
	"id" varchar(36) NOT NULL,
	"patient_id" varchar(36) NOT NULL,
	"doctor_id" varchar(36) NOT NULL,
	"time" TIMESTAMP NOT NULL,
	"complain" TEXT NOT NULL,
	"weight" DECIMAL(10) NOT NULL,
	"blood_pressure" varchar(10) NOT NULL,
	"diagnosis" varchar(255) NOT NULL,
	"treatment" varchar(255) NOT NULL,
	"prescription" varchar(255) NOT NULL,
	"notes" varchar(255) NOT NULL,
	CONSTRAINT "medical_records_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "clinics" (
	"id" varchar(36) NOT NULL,
	"doctor_id" varchar(36) NOT NULL,
	"name" varchar(255) NOT NULL,
	"room" varchar(10) NOT NULL,
	"daily_quota" integer NOT NULL,
	CONSTRAINT "clinics_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "registration_queues" (
	"id" varchar(36) NOT NULL,
	"clinic_id" varchar(36) NOT NULL,
	"time" TIMESTAMP(25) NOT NULL,
	"status" varchar(20) NOT NULL,
	CONSTRAINT "registration_queues_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "clinic_queues" (
	"id" varchar(36) NOT NULL,
	"registration_id" varchar(36) NOT NULL,
	"patient_id" varchar(36) NOT NULL,
	"clinic_id" varchar(36) NOT NULL,
	"status" varchar(20) NOT NULL,
	CONSTRAINT "clinic_queues_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "transactions" (
	"id" varchar(36) NOT NULL,
	"medical_record_id" varchar(36) NOT NULL,
	"clinic_queue_id" varchar(36) NOT NULL,
	"total_cost" FLOAT NOT NULL,
	"status" varchar(20) NOT NULL,
	CONSTRAINT "transactions_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "detail_transactions" (
	"id" varchar(36) NOT NULL,
	"transaction_id" varchar(36) NOT NULL,
	"item" varchar(255) NOT NULL,
	"cost" FLOAT NOT NULL,
	CONSTRAINT "detail_transactions_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);




ALTER TABLE "doctors" ADD CONSTRAINT "doctors_fk0" FOREIGN KEY ("employee_id") REFERENCES "employess"("id");

ALTER TABLE "pharmacists" ADD CONSTRAINT "pharmacists_fk0" FOREIGN KEY ("employee_id") REFERENCES "employess"("id");

ALTER TABLE "patients" ADD CONSTRAINT "patients_fk0" FOREIGN KEY ("regist_by") REFERENCES "employess"("id");

ALTER TABLE "medical_records" ADD CONSTRAINT "medical_records_fk0" FOREIGN KEY ("patient_id") REFERENCES "patients"("id");
ALTER TABLE "medical_records" ADD CONSTRAINT "medical_records_fk1" FOREIGN KEY ("doctor_id") REFERENCES "doctors"("id");

ALTER TABLE "clinics" ADD CONSTRAINT "clinics_fk0" FOREIGN KEY ("doctor_id") REFERENCES "doctors"("id");

ALTER TABLE "registration_queues" ADD CONSTRAINT "registration_queues_fk0" FOREIGN KEY ("clinic_id") REFERENCES "clinics"("id");

ALTER TABLE "clinic_queues" ADD CONSTRAINT "clinic_queues_fk0" FOREIGN KEY ("registration_id") REFERENCES "registration_queues"("id");
ALTER TABLE "clinic_queues" ADD CONSTRAINT "clinic_queues_fk1" FOREIGN KEY ("patient_id") REFERENCES "patients"("id");
ALTER TABLE "clinic_queues" ADD CONSTRAINT "clinic_queues_fk2" FOREIGN KEY ("clinic_id") REFERENCES "clinics"("id");

ALTER TABLE "transactions" ADD CONSTRAINT "transactions_fk0" FOREIGN KEY ("medical_record_id") REFERENCES "medical_records"("id");
ALTER TABLE "transactions" ADD CONSTRAINT "transactions_fk1" FOREIGN KEY ("clinic_queue_id") REFERENCES "clinic_queues"("id");

ALTER TABLE "detail_transactions" ADD CONSTRAINT "detail_transactions_fk0" FOREIGN KEY ("transaction_id") REFERENCES "transactions"("id");











