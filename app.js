// inisiasi server express
const express = require("express");
const app = express();
const port = 3000;

// inisiasi fungsi JSON di server agar dapat menerima request body yang berupa JSON
app.use(express.json());

// inisiasi koneksi ke postgreSQL
const { Pool } = require("pg");

const pool = new Pool({
  host: "localhost",
  port: 5432,
  user: "postgres",
  password: "rahasia",
  max: 20,
  database: "bootcamp",
  idleTimeoutMillis: 30000,
  connectionTimeoutMillis: 2000,
});

// route
// fungsi dibawah ini menerima request dari GET localhost:3000/
app.get("/", (req, res) => {
  res.send("Halo kawan sekalian sekumpulan,k hehe");
});

// insert data employee
app.post("/employees", async (req, res) => {
  const { name } = req.body;
  console.log(name);
  // query disini
  res.send({ message: "Berhasil menyimpan data employees", data: name });
});
// insert data doctors
// insert data doctors

// fungsi dibawah ini menerima request dari GET localhost:3000/employees
app.get("/employees", async (req, res) => {
  try {
    const { rows: data } = await pool.query(
      "select * from clinique.employess e "
    );
    res.send({ message: "Berhasil mengambil data karyawan", data });
  } catch (error) {
    console.log(error);
    res.send(error);
  }
});

app.get("/patients", (req, res) => {
  res.send({ message: "Berhasil mengambil data pasien", data });
});

// jalanin server
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
